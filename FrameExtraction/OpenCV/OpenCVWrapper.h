//
//  OpenCVWrapper.h
//  FrameExtraction
//
//  Created by Eugene G on 5/5/18.
//  Copyright © 2018 Eugene G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject
//- (void)isThisWorking;
- (UIImage *) processImage:(UIImage *)image;

@end

