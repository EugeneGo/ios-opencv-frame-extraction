//
//  ViewController.swift
//  FrameExtraction
//
//  Created by Eugene G on 5/5/18.
//  Copyright © 2018 Eugene G. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var frameExtractor: FrameExtractor!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("Failed Function: \(#function)")
    }
}

extension ViewController: FrameExtractorDelegate {
    func captured(image: UIImage) {
        
        //OpenCV
        let openCVWrapper = OpenCVWrapper()
        self.imageView.image = openCVWrapper.processImage(image)
        
//        imageView.image = image
    }
}
